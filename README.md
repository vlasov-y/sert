Site Entrypoint Rotation Tools
===

```
        ┓━┓┳━┓┳━┓┏┓┓
        ┗━┓┣━ ┃┳┛ ┃ 
        ━━┛┻━┛┇┗┛ ┇ 
┐─┐o┌┐┐┬─┐  ┬─┐┌┐┐┌┐┐┬─┐┐ ┬┬─┐┌─┐o┌┐┐┌┐┐
└─┐│ │ ├─   ├─ │││ │ │┬┘└┌┘│─┘│ │││││ │ 
──┘┘ ┘ ┴─┘  ┴─┘┘└┘ ┘ ┘└┘ ┘ ┘  ┘─┘┘┘└┘ ┘ 
┬─┐┌─┐┌┐┐┬─┐┌┐┐o┌─┐┌┐┐  ┌┐┐┌─┐┌─┐┬  ┐─┐
│┬┘│ │ │ │─┤ │ ││ ││││   │ │ ││ ││  └─┐
┘└┘┘─┘ ┘ ┘ ┘ ┘ ┘┘─┘┘└┘   ┘ ┘─┘┘─┘┘─┘──┘
```

- [Site Entrypoint Rotation Tools](#site-entrypoint-rotation-tools)
  - [Architecture](#architecture)
    - [Wall](#wall)
    - [Chain](#chain)
      - [Benchmark (commit 9a2f9066e)](#benchmark-commit-9a2f9066e)
  - [Rules](#rules)
  - [Rotation](#rotation)
    - [Drift explanation](#drift-explanation)
  - [Plan](#plan)
  - [Thoughts](#thoughts)
  - [Release Notes](#release-notes)
    - [Version 1.0](#version-10)
  - [Contributing](#contributing)
    - [Setup](#setup)
  - [Requirements](#requirements)
  - [Providers](#providers)
  - [Modules](#modules)
  - [Resources](#resources)
  - [Inputs](#inputs)
  - [Outputs](#outputs)

## Architecture

### Wall

**Aim**: frequent changes of site DNS records to avoid being blocked.  
**Structure**: Many *cheap* and *simple* servers on *different clouds* that do the same DNAT using plain iptables all traffic according to the rules.

```mermaid
graph LR
  subgraph Fronend DNS
    entry(DNS example.com)
  end

  subgraph Proxying DNAT
    s1(Proxy Server #1)
    s2(Proxy Server #2)
    s3(Proxy Server #3)
  end

  subgraph Infrastructure DNS
    target(my-site.secret.com)
  end

  entry --> s1
  entry --> s2
  entry --> s3
  s1 --> target
  s2 --> target
  s3 --> target
```

### Chain

**Aim**: mixing traffic between many servers in many clouds - Tor-like personal network.  
**Structure**: chain of walls with exactly 1 server each.

```mermaid
graph LR
  subgraph Fronend DNS
    entry(DNS example.com)
  end

  subgraph Proxying DNAT
    s1(Proxy Server #1) -->
    s2(Proxy Server #2) -->
    s3(Proxy Server #3)
  end

  subgraph Infrastructure DNS
    target(my-site.secret.com)
  end

  entry --> s1
  s3 --> target
```

#### Benchmark (commit 9a2f9066e)

Check benchmark folder for details.

## Rules

Supported protocols: all that iptables support, but also have to be supported by Cloud's firewall/security-group.

Example rules

- *tcp:80 -> example.com:80* 
- *udp:500-700 -> something.else:10500-10700*

## Rotation

Rotation makes serves to obtain new public IPs from cloud provider and hence **drift domain name records**.
Server can be rotated according to schedule. You can setup time offset in minutes/hours/day, after reaching which server will be recreated (in order to obtain new public IP). Also, **drift can be set** in percent.

### Drift explanation

Imagine an input.

- 3 servers
- rotation time 20 hours
- drift 25%

What will be done.

- random drift will be generated for each server first in limits `[1;25]`.
- random direction will be taken `+` or `-` in order to understand either it will be increasing or decreasing of the time
- calculated time offset will be fixed per server and never change in case, unless you manually taint appropriate random objects

Result (example of random).

```yaml
server-1:
  drift: +17%
  time: 23.4h
server-2:
  drift: -5%
  time: 19h
server-3:
  drift: -21%
  time: 15.8h
```

## Plan

- [x] servers creation
- [x] iptables forwarding through hosts
- [x] forwarding for both ipv4 and domain
- [x] hcloud provider support
- [ ] ~~enable/disable rules per group~~ It overloads templating significantly. Better to invoke module twice with different rules
- [x] rotation using time: minutes, hours, days
- [x] rotation drift adding uniq random to the time
- [x] cloudflare domain management using cloudflare - writing all server's IPs
- [x] make layer system of proxies
- [x] define layers ~~but shuffletheir nodes~~ and build a chain of width = 1
- [x] add chain shuffle option
- [ ] ~~create before destroy for servers and wait for DNS propagation solution have to be imagined first~~
- [ ] support AWS provider
- [ ] add optional AWS VPC creation
- [ ] calculate server/cloud/total prices - https://docs.hetzner.cloud/#servers-get-a-server
- [ ] write GitLab CI and documentation
- [ ] use [healthchecks.io](https://healthchecks.io) as IDS signal system

## Thoughts

1. I cannot find a way to define a constraint and destroy server only after its replacement is already propagated in DNS. Way that will work, but it is the last way: make cloudflare changes from server intself and create /tmp/ready, that will be waited from remote-exec. Together with create_before_destroy that will work. But how to delete record of the server that is going to be deleted?
2. What about chaining layers, pointing one after another? **DONE**
3. Can configure a system to alert to healthcheck.io in case of many resons:
   1. SSH intrussion - new SSH session. Info to include:
      1. username
      2. from_ip
      3. auth_method
      4. timestamp
      5. command executed
      6. is port forward (maybe just disable port_forward)
   2. Chain is broken when server i failes to send traffic to i+1. Info to include:
      1. IP of the target server
      2. iptables error message
   3. High CPU/Memory consumption. Cases:
      1. network usage high
      2. ram usage high
      3. cpu usage high
   4. DNS resolution fails for the target domain. Info to include.
      1. domain

   In case of intrusion - chaing have to be recreated asap, without investigation.
  
## Release Notes

### Version 1.0

- Hetzner Cloud support
  - Firewall rules
  - Instance creation
- cloud-init is used to bootstrap the stack
- sert.sh script controls *iptables* basing on */etc/sert-rules.json*

## Contributing

### Setup

1. Install requirements for [pre-commit-terraform](https://github.com/antonbabenko/pre-commit-terraform)
   1. [pre-commit](https://pre-commit.com/#install)
   2. [checkov](https://github.com/bridgecrewio/checkov)
   3. [terraform-docs](https://github.com/terraform-docs/terraform-docs)
   4. [tflint](https://github.com/terraform-linters/tflint)
2. Run `pre-commit install`


<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.6.3 |
| <a name="requirement_acme"></a> [acme](#requirement\_acme) | < 3.0.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | < 6.0.0 |
| <a name="requirement_cloudflare"></a> [cloudflare](#requirement\_cloudflare) | < 5.0.0 |
| <a name="requirement_hcloud"></a> [hcloud](#requirement\_hcloud) | < 2.0.0 |
| <a name="requirement_local"></a> [local](#requirement\_local) | < 3.0.0 |
| <a name="requirement_random"></a> [random](#requirement\_random) | < 4.0.0 |
| <a name="requirement_tls"></a> [tls](#requirement\_tls) | < 5.0.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_acme"></a> [acme](#provider\_acme) | 2.18.0 |
| <a name="provider_aws"></a> [aws](#provider\_aws) | 5.26.0 |
| <a name="provider_cloudflare"></a> [cloudflare](#provider\_cloudflare) | 4.19.0 |
| <a name="provider_random"></a> [random](#provider\_random) | 3.5.1 |
| <a name="provider_tls"></a> [tls](#provider\_tls) | 4.0.4 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_aws"></a> [aws](#module\_aws) | ./modules/clouds/aws | n/a |
| <a name="module_cloudinit"></a> [cloudinit](#module\_cloudinit) | ./modules/cloudinit | n/a |
| <a name="module_hcloud"></a> [hcloud](#module\_hcloud) | ./modules/clouds/hcloud | n/a |
| <a name="module_rotation"></a> [rotation](#module\_rotation) | ./modules/drift | n/a |
| <a name="module_xray_config"></a> [xray\_config](#module\_xray\_config) | ./modules/xray-config | n/a |

## Resources

| Name | Type |
|------|------|
| [acme_certificate.entrypoint](https://registry.terraform.io/providers/vancluever/acme/latest/docs/resources/certificate) | resource |
| [acme_registration.this](https://registry.terraform.io/providers/vancluever/acme/latest/docs/resources/registration) | resource |
| [cloudflare_record.aws](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/record) | resource |
| [cloudflare_record.hcloud](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/record) | resource |
| [random_shuffle.mesh](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/shuffle) | resource |
| [random_string.server_id](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/string) | resource |
| [random_string.websocket_uri_path](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/string) | resource |
| [random_uuid.xray](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/uuid) | resource |
| [tls_cert_request.chain](https://registry.terraform.io/providers/hashicorp/tls/latest/docs/resources/cert_request) | resource |
| [tls_locally_signed_cert.chain](https://registry.terraform.io/providers/hashicorp/tls/latest/docs/resources/locally_signed_cert) | resource |
| [tls_private_key.acme_account](https://registry.terraform.io/providers/hashicorp/tls/latest/docs/resources/private_key) | resource |
| [tls_private_key.ca](https://registry.terraform.io/providers/hashicorp/tls/latest/docs/resources/private_key) | resource |
| [tls_private_key.chain](https://registry.terraform.io/providers/hashicorp/tls/latest/docs/resources/private_key) | resource |
| [tls_self_signed_cert.ca](https://registry.terraform.io/providers/hashicorp/tls/latest/docs/resources/self_signed_cert) | resource |
| [aws_vpc.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/vpc) | data source |
| [cloudflare_zone.this](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/data-sources/zone) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cloudflare_enabled"></a> [cloudflare\_enabled](#input\_cloudflare\_enabled) | Cloudflare switch | `bool` | `false` | no |
| <a name="input_cloudflare_token"></a> [cloudflare\_token](#input\_cloudflare\_token) | Cloudflare token with Zone:Read and DNS:Write permissions | `string` | n/a | yes |
| <a name="input_environment"></a> [environment](#input\_environment) | Name of the environment you deploy | `string` | `"default"` | no |
| <a name="input_hcloud_enabled"></a> [hcloud\_enabled](#input\_hcloud\_enabled) | Hetzner Cloud swtich | `bool` | `false` | no |
| <a name="input_hcloud_token"></a> [hcloud\_token](#input\_hcloud\_token) | Hetzner Cloud project API ReadWrite token | `string` | n/a | yes |
| <a name="input_organization"></a> [organization](#input\_organization) | Name of your company | `string` | `"sert"` | no |
| <a name="input_xray_vless_clients"></a> [xray\_vless\_clients](#input\_xray\_vless\_clients) | VLESS clients to set on the first host in the chain | `list(any)` | `[]` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_ca_cert_pem"></a> [ca\_cert\_pem](#output\_ca\_cert\_pem) | CA we used for issuing server chain certificates |
| <a name="output_domains"></a> [domains](#output\_domains) | Domains created for servers |
| <a name="output_entrypoint"></a> [entrypoint](#output\_entrypoint) | Chain entrypoint domain name |
| <a name="output_mesh"></a> [mesh](#output\_mesh) | Mesh chain |
| <a name="output_websocket_uri_path"></a> [websocket\_uri\_path](#output\_websocket\_uri\_path) | Websocket URI path to use |
| <a name="output_xray_config"></a> [xray\_config](#output\_xray\_config) | XRay configuration for all nodes in the chain |
| <a name="output_xray_config_json"></a> [xray\_config\_json](#output\_xray\_config\_json) | Like xray\_config but in JSON |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
