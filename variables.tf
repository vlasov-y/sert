#  ┐ ┬┬─┐┬─┐o┬─┐┬─┐┬  ┬─┐┐─┐
#  │┌┘│─┤│┬┘││─┤│─││  ├─ └─┐
#  └┘ ┘ ┘┘└┘┘┘ ┘┘─┘┘─┘┴─┘──┘

variable "organization" {
  type        = string
  description = "Name of your company"
  default     = "sert"
}

variable "environment" {
  type        = string
  description = "Name of the environment you deploy"
  default     = "default"
}

#  ┬ ┬┬─┐┌┐┐┌─┐┌┐┐┬─┐┬─┐
#  │─┤├─  │ ┌─┘│││├─ │┬┘
#  ┘ ┴┴─┘ ┘ └─┘┘└┘┴─┘┘└┘

variable "hcloud_enabled" {
  type        = bool
  description = "Hetzner Cloud swtich"
  default     = false
}

variable "hcloud_token" {
  type        = string
  description = "Hetzner Cloud project API ReadWrite token"
  sensitive   = true
}

#  ┌─┐┬  ┌─┐┬ ┐┬─┐┬─┐┬  ┬─┐┬─┐┬─┐
#  │  │  │ ││ ││ │├─ │  │─┤│┬┘├─ 
#  └─┘┘─┘┘─┘┘─┘┘─┘┘  ┘─┘┘ ┘┘└┘┴─┘

variable "cloudflare_enabled" {
  type        = bool
  description = "Cloudflare switch"
  default     = false
}

variable "cloudflare_token" {
  type        = string
  description = "Cloudflare token with Zone:Read and DNS:Write permissions"
  sensitive   = true
}

#  ┐ │┬─┐┬─┐┐ ┬
#  ┌┼┘│┬┘│─┤└┌┘
#  ┘ └┘└┘┘ ┘ ┘ 

variable "xray_vless_clients" {
  type        = list(any)
  description = "VLESS clients to set on the first host in the chain"
  default     = []
}
