#  ┬─┐┬─┐┌┐┐┬─┐┌─┐┌┌┐
#  │┬┘│─┤││││ ││ ││││
#  ┘└┘┘ ┘┘└┘┘─┘┘─┘┘ ┘

resource "random_string" "server_id" {
  for_each = local.servers
  length   = 16
  lower    = true
  upper    = false
  numeric  = true
  special  = false
}

resource "random_uuid" "xray" {
  for_each = local.servers
  keepers = {
    server_id = random_string.server_id[each.key].result
  }
}

resource "random_shuffle" "mesh" {
  input = keys(local.servers)
  keepers = {
    servers = sha256(jsonencode(local.servers))
  }
}

locals {
  websocket_uri_path = "/${random_string.websocket_uri_path.result}" #?ed=2048"
}

resource "random_string" "websocket_uri_path" {
  length  = 32
  lower   = true
  upper   = false
  numeric = true
  special = false
}
