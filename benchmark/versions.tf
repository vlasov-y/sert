#  ┐ ┬┬─┐┬─┐┐─┐o┌─┐┌┐┐┐─┐
#  │┌┘├─ │┬┘└─┐││ ││││└─┐
#  └┘ ┴─┘┘└┘──┘┘┘─┘┘└┘──┘

terraform {
  required_version = ">= 1.6.3"

  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "~> 1.33.1"
    }
    local = {
      source  = "hashicorp/local"
      version = "~> 2.2.2"
    }
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 3.11.0"
    }
  }
}
