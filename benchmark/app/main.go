package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"math"
	"net/http"
	"os/exec"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/push"
)

var (
	pushGatewayURL = flag.String("url", "", "URL of the Prometheus Push gateway")
	runID          = flag.String("run-id", "", "Label to be used as run_id")
	username       = flag.String("username", "", "Username for basic authentication")
	password       = flag.String("password", "", "Password for basic authentication")
)

type HTTPResult struct {
	Min               time.Duration `json:"min"`
	Max               time.Duration `json:"max"`
	Average           time.Duration `json:"avg"`
	StandardDeviation time.Duration `json:"std"`
}

type SpeedTestResult struct {
	Download float64 `json:"download"`
	Upload   float64 `json:"upload"`
	Ping     float64 `json:"ping"`
}

func runHTTPConnectTest(url string, numRequests int) HTTPResult {
	var totalDuration, minDuration, maxDuration time.Duration

	durations := make([]time.Duration, numRequests)

	for i := 0; i < numRequests; i++ {
		startTime := time.Now()

		// HTTP connection test logic (same as before)

		duration := time.Since(startTime)
		durations[i] = duration

		if i == 0 || duration < minDuration {
			minDuration = duration
		}
		if i == 0 || duration > maxDuration {
			maxDuration = duration
		}

		totalDuration += duration
	}

	averageDuration := totalDuration / time.Duration(numRequests)

	var sumSquaredDiffs float64
	for _, duration := range durations {
		diff := float64(duration - averageDuration)
		sumSquaredDiffs += diff * diff
	}
	standardDeviation := time.Duration(math.Sqrt(sumSquaredDiffs / float64(numRequests)))

	return HTTPResult{
		Min:               minDuration,
		Max:               maxDuration,
		Average:           averageDuration,
		StandardDeviation: standardDeviation,
	}
}

func runSpeedTest() (SpeedTestResult, error) {
	cmd := exec.Command("speedtest", "--json")
	output, err := cmd.CombinedOutput()
	if err != nil {
		fmt.Println(string(output))
		return SpeedTestResult{}, fmt.Errorf("error running speedtest: %v", err)
	}

	var result map[string]interface{}
	err = json.Unmarshal(output, &result)
	if err != nil {
		return SpeedTestResult{}, fmt.Errorf("error parsing speedtest output: %v", err)
	}

	// Extracting relevant fields from speedtest output
	download, _ := result["download"].(float64)
	upload, _ := result["upload"].(float64)
	ping, _ := result["ping"].(float64)

	return SpeedTestResult{
		Download: download,
		Upload:   upload,
		Ping:     ping,
	}, nil
}

func pushMetricsToPushGateway(url, label, username, password string, httpResult HTTPResult, speedTestResult SpeedTestResult) error {
	// Define Prometheus metrics
	httpConnectMin := prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "sert_http_connect_min",
		Help: "Minimum duration of HTTP connection test",
	})

	httpConnectMax := prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "sert_http_connect_max",
		Help: "Maximum duration of HTTP connection test",
	})

	httpConnectAvg := prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "sert_http_connect_avg",
		Help: "Average duration of HTTP connection test",
	})

	httpConnectStd := prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "sert_http_connect_std",
		Help: "Standard deviation of HTTP connection test",
	})

	speedTestDownload := prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "sert_speedtest_download",
		Help: "Download speed of SpeedTest",
	})

	speedTestUpload := prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "sert_speedtest_upload",
		Help: "Upload speed of SpeedTest",
	})

	speedTestPing := prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "sert_speedtest_ping",
		Help: "Ping duration of SpeedTest",
	})

	// Set metric values
	httpConnectMin.Set(float64(httpResult.Min) / float64(time.Millisecond))
	httpConnectMax.Set(float64(httpResult.Max) / float64(time.Millisecond))
	httpConnectAvg.Set(float64(httpResult.Average) / float64(time.Millisecond))
	httpConnectStd.Set(float64(httpResult.StandardDeviation) / float64(time.Millisecond))

	speedTestDownload.Set(speedTestResult.Download)
	speedTestUpload.Set(speedTestResult.Upload)
	speedTestPing.Set(speedTestResult.Ping)

	// Push metrics to Prometheus Push Gateway with basic authentication
	pusher := push.New(*pushGatewayURL, "sert_http_connect").
		Collector(httpConnectMin).
		Collector(httpConnectMax).
		Collector(httpConnectAvg).
		Collector(httpConnectStd).
		Collector(speedTestDownload).
		Collector(speedTestUpload).
		Collector(speedTestPing).
		Grouping("run_id", label)

	if username != "" && password != "" {
		pusher = pusher.BasicAuth(username, password)
	}

	client := &http.Client{
		Transport: &http.Transport{
			Proxy: nil,
		},
	}
	if err := pusher.Client(client).Add(); err != nil {
		return fmt.Errorf("error adding metrics to pusher: %v", err)
	}

	return nil
}

func main() {
	flag.Parse()

	if *pushGatewayURL == "" || *runID == "" {
		fmt.Println("Both 'url' and 'run-id' flags are required.")
		return
	}

	url := "http://google.com"
	numHTTPRequests := 100

	// Run HTTP connection test
	fmt.Println("Running HTTP test.")
	httpResult := runHTTPConnectTest(url, numHTTPRequests)

	// Run SpeedTest
	fmt.Println("Running speed test.")
	speedTestResult, err := runSpeedTest()
	if err != nil {
		fmt.Printf("Error running speedtest: %v\n", err)
		return
	}

	// Push metrics to Prometheus Push Gateway
	fmt.Println("Pushing metrics.")
	err = pushMetricsToPushGateway(*pushGatewayURL, *runID, *username, *password, httpResult, speedTestResult)
	if err != nil {
		fmt.Printf("Error pushing metrics to Push Gateway: %v\n", err)
		return
	}

	fmt.Println("Metrics pushed successfully.")
}
