#  ┬─┐┬─┐┌─┐┐ ┬o┬─┐┬─┐┬─┐┐─┐
#  │─┘│┬┘│ ││┌┘││ │├─ │┬┘└─┐
#  ┘  ┘└┘┘─┘└┘ ┘┘─┘┴─┘┘└┘──┘

provider "hcloud" {
  token = var.hcloud_token
}

provider "cloudflare" {
  api_token = var.cloudflare_token
}

#  ┌─┐┬ ┬┬─┐o┌┐┐
#  │  │─┤│─┤││││
#  └─┘┘ ┴┘ ┘┘┘└┘

module "chain" {
  source                 = "../modules/sert/chain"
  name                   = "benchmark-chain"
  root_path              = abspath(path.module)
  sert_wall_module_path  = "../modules/sert/wall"
  cloudflare_module_path = "../modules/cloudflare/records"

  hcloud_enabled = true
  nodes          = [for i in range(var.length) : "hcloud"]
  rotation       = [for i in range(var.length) : { hours = 1 }]
  shuffle        = false

  spec = {
    hcloud = {
      instance_type = "cpx11"
    }
  }

  rules = {
    tcp = {
      port     = 80
      protocol = "tcp"
      dnat = {
        target = "vpn.example.com"
      }
    }
    udp = {
      port     = 80
      protocol = "udp"
      dnat = {
        target = "vpn.example.com"
      }
    }
  }

  cloudflare_enabled = true
  cloudflare_zone    = "example.com"
  cloudflare_domain  = "test.example.com"

  tags = local.tags
}
