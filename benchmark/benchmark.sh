#!/usr/bin/env bash
set -eo pipefail

DIRECT="$1"
CHAIN="$2"
MIN_LENGTH=1
MAX_LENGTH=5
OUTPUT="$(pwd)/logs"
TFVARS=benchmark.auto.tfvars
# jump to terraform folder
cd ..

main() {
  mkdir -p "$OUTPUT"
  echo 'info: testing direct bandwidth'
  # benchmark "$DIRECT" -P 10 | tee "${OUTPUT}/direct.tcp"
  # benchmark "$DIRECT" -P 2 -b 50M --udp | tee "${OUTPUT}/direct.udp"
  LENGTH="$MIN_LENGTH"
  while [ "$LENGTH" -le "$MAX_LENGTH" ]; do
    scale "$LENGTH"
    benchmark "$CHAIN" -P 10 | tee "${OUTPUT}/chain-${LENGTH}.tcp"
    benchmark "$CHAIN" -P 2 -b 50M --udp | tee "${OUTPUT}/chain-${LENGTH}.udp"
    LENGTH="$((LENGTH+1))"
  done
}

benchmark() {
  ADDRESS="$1"
  shift 1
  iperf3 -c "$ADDRESS" -p 80 -t 100 "$@" || true
}

scale() {
  export TF_VAR_chain_length="$1"
  echo "info: scaling to $1"
  # will generate new chain template
  terraform apply -auto-approve
  # and now we have to apply it running once again
  terraform apply -auto-approve
  # check plan has no changes
  if ! terraform plan -detailed-exitcode; then
    echo 'error: plan must be empty, but it is not' >&2
    exit 2
  fi
  # wait for DNS resolution
  echo "info: waiting some time for DNS"
  sleep 30
  if ! dig +short "$CHAIN" @1.1.1.1 | grep -qE '.+'; then
    echo "error: failed to resolve $CHAIN" >&2
    exit 3
  fi
}

main
