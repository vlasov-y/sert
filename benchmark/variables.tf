#  ┐ ┬┬─┐┬─┐o┬─┐┬─┐┬  ┬─┐┐─┐
#  │┌┘│─┤│┬┘││─┤│─││  ├─ └─┐
#  └┘ ┘ ┘┘└┘┘┘ ┘┘─┘┘─┘┴─┘──┘

variable "chain_length" {
  type        = number
  description = "Count of servers in the chain"
}

#  ┬ ┬┬─┐┌┐┐┌─┐┌┐┐┬─┐┬─┐
#  │─┤├─  │ ┌─┘│││├─ │┬┘
#  ┘ ┴┴─┘ ┘ └─┘┘└┘┴─┘┘└┘

variable "hcloud_token" {
  type        = string
  description = "Hetzner Cloud project API ReadWrite token"
  sensitive   = true
}

#  ┌─┐┬  ┌─┐┬ ┐┬─┐┬─┐┬  ┬─┐┬─┐┬─┐
#  │  │  │ ││ ││ │├─ │  │─┤│┬┘├─ 
#  └─┘┘─┘┘─┘┘─┘┘─┘┘  ┘─┘┘ ┘┘└┘┴─┘

variable "cloudflare_token" {
  type        = string
  description = "Cloudflare token with Zone:Read and DNS:Write permissions"
  sensitive   = true
}
