Chaining benchmark
===

## Over the Internet

I have a laptop and want to know how many bandwidth will be lost because of proxying chain. I have ~100 Mbits/sec Internet at home. Target and chain servers are both Hetzner VPS with shared CPUs. I have taken this case, because it is the cheapeast and one that has been in my mind during the development. Yes, it is possible to take dedicated-CPU VMs, and use not a laptop in Ukraine, but another one VM in the same region in the cloud, but that is not a use case.

### Servers specification

| Host       | Location                     | Distro                 | Kernel                                  | Version                   | Args TCP              | Args UDP                          |
| ---------- | ---------------------------- | ---------------------- | --------------------------------------- | ------------------------- | --------------------- | --------------------------------- |
| Client     | Kharkiv, Ukraine             | Manjaro Linux          | 5.15.28-1-MANJARO                       | iperf 3.11 (cJSON 1.7.13) | `-cp 80 -P 10 -t 100` | `-cp 80 -P 2 -b 50M -t 100 --udp` |
| Server     | Hetzner, eu-central, Germany | CentOS VPS CX11        | 3.10.0-1160.59.1.el7.centos.plus.x86_64 | iperf 3.1.7               | `-sp 80`              | `-sp 80`                          |
| Chain node | Hetzner, eu-central, Germany | Ubuntu 20.04 VPS CPX11 | 5.4.0-96-generic                        | iptables v1.8.4 (legacy)  | N/A                   | N/A                               |

### Results

There are a few TCP errors and they have pretty random. I think, errors can be ignored because of shared vCPUs on VMs, and some other possible noise over the network. That are definitely not errors that are produced because of proxying itself and only.  
Results are great, no bandwidth decrease for chain up to 5. More than five have not been tested :) If you want, you can try, but take a note, that Hetzner has limitation of 10 servers per account by default.

#### TCP

| Chain length | Side     | Bitrate, Mbits/sec | Retries, # | Bitrate % | Retries % |
| ------------ | -------- | ------------------ | ---------- | --------- | --------- |
| direct       | Sender   | 94.7               | 189        | 100%      | 100%      |
| 1            | Sender   | 94.7               | 1          | 100%      | 1%        |
| 2            | Sender   | 94.8               | 131        | 100%      | 69%       |
| 3            | Sender   | 94.7               | 0          | 100%      | 0%        |
| 4            | Sender   | 94.8               | 90         | 100%      | 48%       |
| 5            | Sender   | 94.8               | 6          | 100%      | 3%        |
| direct       | Reciever | 93.9               | N/A        | 100%      | N/A       |
| 1            | Reciever | 93.9               | N/A        | 100%      | N/A       |
| 2            | Reciever | 93.9               | N/A        | 100%      | N/A       |
| 3            | Reciever | 93.9               | N/A        | 100%      | N/A       |
| 4            | Reciever | 93.9               | N/A        | 100%      | N/A       |
| 5            | Reciever | 93.9               | N/A        | 100%      | N/A       |

#### UDP

| Chain length | Side     | Bitrate, Mbits/sec | Jitter, ms | Bitrate % | Jitter % | Lost/Total Datagrams |
| ------------ | -------- | ------------------ | ---------- | --------- | -------- | -------------------- |
| direct       | Sender   | 100                | 0          | 100%      | N/A      | 0/862995 (0%)        |
| 1            | Sender   | 100                | 0          | 100%      | N/A      | 0/863247 (0%)        |
| 2            | Sender   | 100                | 0          | 100%      | N/A      | 0/862981 (0%)        |
| 3            | Sender   | 100                | 0          | 100%      | N/A      | 0/863026 (0%)        |
| 4            | Sender   | 100                | 0          | 100%      | N/A      | 0/863015 (0%)        |
| 5            | Sender   | 100                | 0          | 100%      | N/A      | 0/863057 (0%)        |
| direct       | Reciever | 95.6               | 0.349      | 100%      | 100%     | 37979/862920 (4.4%)  |
| 1            | Reciever | 95.6               | 0.340      | 100%      | 97%      | 38212/863162 (4.4%)  |
| 2            | Reciever | 95.6               | 0.432      | 100%      | 124%     | 37269/862911 (4.3%)  |
| 3            | Reciever | 95.5               | 0.335      | 100%      | 96%      | 38425/862941 (4.5%)  |
| 4            | Reciever | 95.6               | 0.344      | 100%      | 99%      | 38019/862941 (4.4%)  |
| 5            | Reciever | 95.5               | 0.332      | 100%      | 95%      | 38173/862973 (4.4%)  |

## Notes

Converted spreadsheet to Markdown [here](https://tabletomarkdown.com/convert-spreadsheet-to-markdown/). Greate site, thanks!

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.6.3 |
| <a name="requirement_cloudflare"></a> [cloudflare](#requirement\_cloudflare) | ~> 3.11.0 |
| <a name="requirement_hcloud"></a> [hcloud](#requirement\_hcloud) | ~> 1.33.1 |
| <a name="requirement_local"></a> [local](#requirement\_local) | ~> 2.2.2 |

## Providers

No providers.

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_chain"></a> [chain](#module\_chain) | ../modules/sert/chain | n/a |

## Resources

No resources.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_chain_length"></a> [chain\_length](#input\_chain\_length) | Count of servers in the chain | `number` | n/a | yes |
| <a name="input_cloudflare_token"></a> [cloudflare\_token](#input\_cloudflare\_token) | Cloudflare token with Zone:Read and DNS:Write permissions | `string` | n/a | yes |
| <a name="input_hcloud_token"></a> [hcloud\_token](#input\_hcloud\_token) | Hetzner Cloud project API ReadWrite token | `string` | n/a | yes |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
