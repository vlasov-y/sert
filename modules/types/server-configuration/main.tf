variable "input" {
  type        = string
  description = "Valid JSON/YAML"
}

locals {
  decoded = yamldecode(var.input)
}

output "output" {
  description = "Result server configuration"
  value = {
    location  = lookup(decoded, "location", null)
    size      = lookup(decoded, "size", null)
    image     = lookup(decoded, "image", null)
    user_data = lookup(decoded, "user_data", null)
  }
}

terraform {
  required_version = ">= 1.6.3"
}
