variable "input" {
  type        = string
  description = "Valid JSON/YAML"
}

locals {
  decoded = yamldecode(var.input)
}

output "output" {
  description = "Result drift configuration"
  value = {
    minutes = lookup(decoded, "minutes", 0)
    hours   = lookup(decoded, "hours", 0)
    days    = lookup(decoded, "days", 0)
    percent = lookup(decoded, "percent", 0)
  }
}

terraform {
  required_version = ">= 1.6.3"
}
