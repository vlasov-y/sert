variable "input" {
  type        = string
  description = "Valid JSON/YAML"
}

locals {
  decoded = yamldecode(var.input)
}

output "output" {
  description = "Result server configuration"
  value = {
    type             = lookup(decoded, "type", null)
    from_port        = lookup(decoded, "from_port", 0)
    to_port          = lookup(decoded, "to_port", 0)
    protocol         = lookup(decoded, "protocol", "tcp")
    description      = lookup(decoded, "description", "")
    cidr_blocks_ipv4 = lookup(decoded, "cidr_blocks_ipv4", [])
    cidr_blocks_ipv6 = lookup(decoded, "cidr_blocks_ipv6", [])
  }
}

terraform {
  required_version = ">= 1.6.3"
}
