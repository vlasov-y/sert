#  ┌─┐┬  ┌─┐┬ ┐┬─┐  o┌┐┐o┌┐┐
#  │  │  │ ││ ││ │──│││││ │ 
#  └─┘┘─┘┘─┘┘─┘┘─┘  ┘┘└┘┘ ┘ 

output "result" {
  description = "Valid cloudinit script"
  value       = format("#cloud-config\n%s", yamlencode(local.cloud_init))
}
