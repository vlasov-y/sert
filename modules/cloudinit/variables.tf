#  ┌─┐┬  ┌─┐┬ ┐┬─┐  o┌┐┐o┌┐┐
#  │  │  │ ││ ││ │──│││││ │ 
#  └─┘┘─┘┘─┘┘─┘┘─┘  ┘┘└┘┘ ┘ 

variable "username" {
  type        = string
  description = "Linux user to create"
  default     = "user"
}

variable "password" {
  type        = string
  description = "Linux user password hash from mkpasswd"
}

variable "ssh_port" {
  type        = number
  description = "SSH port"
  default     = 22
}

variable "ssh_public_keys" {
  type        = list(string)
  description = "Public SSH keys to set"
  default     = []
}

variable "scripts" {
  type        = map(string)
  description = "List of scripts in plain text to create on the server. Key is script full path, value is content."
  default     = {}
}

variable "files" {
  type        = map(string)
  description = "Like scripts, but will have 0600 permissions instead."
  default     = {}
}


variable "runcmd" {
  type        = list(string)
  description = "Extra shell commands to execute during init"
  default     = []
}
