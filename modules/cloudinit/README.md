# cloudinit

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.6.3 |

## Providers

No providers.

## Modules

No modules.

## Resources

No resources.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_files"></a> [files](#input\_files) | Like scripts, but will have 0600 permissions instead. | `map(string)` | `{}` | no |
| <a name="input_password"></a> [password](#input\_password) | Linux user password hash from mkpasswd | `string` | n/a | yes |
| <a name="input_runcmd"></a> [runcmd](#input\_runcmd) | Extra shell commands to execute during init | `list(string)` | `[]` | no |
| <a name="input_scripts"></a> [scripts](#input\_scripts) | List of scripts in plain text to create on the server. Key is script full path, value is content. | `map(string)` | `{}` | no |
| <a name="input_ssh_port"></a> [ssh\_port](#input\_ssh\_port) | SSH port | `number` | `22` | no |
| <a name="input_ssh_public_keys"></a> [ssh\_public\_keys](#input\_ssh\_public\_keys) | Public SSH keys to set | `list(string)` | `[]` | no |
| <a name="input_username"></a> [username](#input\_username) | Linux user to create | `string` | `"user"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_result"></a> [result](#output\_result) | Valid cloudinit script |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
