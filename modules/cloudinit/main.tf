#  ┌─┐┬  ┌─┐┬ ┐┬─┐  o┌┐┐o┌┐┐
#  │  │  │ ││ ││ │──│││││ │ 
#  └─┘┘─┘┘─┘┘─┘┘─┘  ┘┘└┘┘ ┘ 

locals {
  cloud_init = {
    users = [{
      name                = var.username
      sudo                = "ALL=(ALL) NOPASSWD:ALL"
      shell               = "/bin/bash"
      ssh_authorized_keys = var.ssh_public_keys
    }]
    packages         = ["curl"]
    packages_update  = true
    packages_upgrade = true
    write_files = concat(
      [
        for path, content in var.files :
        {
          path        = path
          owner       = "root:root"
          permissions = "0600"
          encoding    = "b64"
          content     = base64encode(content)
        }
      ],
      [
        for path, content in var.scripts :
        {
          path        = path
          owner       = "root:root"
          permissions = "0700"
          encoding    = "b64"
          content     = base64encode(content)
        }
      ]
    )
    runcmd = concat([
      "printf '${var.username}:${var.password}' | chpasswd -e",
      "sed -ri 's/^#?Port.+/Port ${var.ssh_port}/g' /etc/ssh/sshd_config",
      "sed -ri 's/^#?PermitRootLogin.+/PermitRootLogin no/g' /etc/ssh/sshd_config",
      "sed -ri 's/^#?PasswordAuthentication.+/PasswordAuthentication yes/g' /etc/ssh/sshd_config",
      "systemctl restart sshd.service",
      "curl -sSL https://get.docker.com | sh -",
      "systemctl enable --now docker.service",
    ], var.runcmd)
  }
}
