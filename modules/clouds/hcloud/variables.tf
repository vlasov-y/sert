#  ┐ ┬┬─┐┬─┐o┬─┐┬─┐┬  ┬─┐┐─┐
#  │┌┘│─┤│┬┘││─┤│─││  ├─ └─┐
#  └┘ ┘ ┘┘└┘┘┘ ┘┘─┘┘─┘┴─┘──┘

variable "name" {
  type        = string
  description = "Name prefix"
}

variable "create" {
  type    = bool
  default = true
}

variable "configuration" {
  type = object({
    servers        = map(any)
    firewall_rules = list(any)
    tags           = optional(map(string))
  })
  description = "Unified configuration in valid YAML/JSON"
}
