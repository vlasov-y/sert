#  ┬ ┬┬─┐┌┐┐┌─┐┌┐┐┬─┐┬─┐
#  │─┤├─  │ ┌─┘│││├─ │┬┘
#  ┘ ┴┴─┘ ┘ └─┘┘└┘┴─┘┘└┘

locals {
  tags = lookup(var.configuration, "tags", {})
}

#  ┬─┐o┬─┐┬─┐┐ ┬┬─┐┬  ┬  
#  ├─ ││┬┘├─ ││││─┤│  │  
#  ┘  ┘┘└┘┴─┘└┴┘┘ ┘┘─┘┘─┘

locals {
  firewall_rules = {
    for name, spec in var.configuration.firewall_rules :
    name => spec if var.create
  }
}

resource "hcloud_firewall" "this" {
  count = var.create ? 1 : 0
  name  = var.name

  dynamic "rule" {
    for_each = local.firewall_rules
    content {
      direction = rule.value.type == "ingress" ? "in" : "out"
      port = rule.value.from_port == rule.value.to_port == 0 ? "all" : (
        rule.value.from_port == rule.value.to_port ? tostring(rule.value.from_port) :
        "${rule.value.from_port}-${rule.value.to_port}"
      )
      protocol    = rule.value.protocol
      source_ips  = concat(rule.value.cidr_blocks_ipv4, rule.value.cidr_blocks_ipv6)
      description = coalesce(rule.value.description, rule.key)
    }
  }

  labels = local.tags
}

resource "hcloud_firewall_attachment" "this" {
  count       = var.create ? 1 : 0
  firewall_id = hcloud_firewall.this[0].id
  server_ids  = [for key, value in hcloud_server.this : value.id]
}

#  ┐─┐┬─┐┬─┐┐ ┬┬─┐┬─┐
#  └─┐├─ │┬┘│┌┘├─ │┬┘
#  ──┘┴─┘┘└┘└┘ ┴─┘┘└┘

locals {
  servers = {
    for server, configuration in var.configuration.servers :
    lookup(configuration, "name", "${var.name}-${server}") => merge(
      {
        location = "nbg1"
        size     = "cpx11"
        image    = "ubuntu-22.04"
      },
      configuration
    )
    if var.create
  }
}

resource "hcloud_server" "this" {
  for_each    = local.servers
  name        = each.key
  image       = each.value.image
  server_type = each.value.size
  location    = each.value.location
  user_data   = each.value.user_data
  labels      = local.tags
}
