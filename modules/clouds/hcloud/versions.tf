#  ┐ ┬┬─┐┬─┐┐─┐o┌─┐┌┐┐┐─┐
#  │┌┘├─ │┬┘└─┐││ ││││└─┐
#  └┘ ┴─┘┘└┘──┘┘┘─┘┘└┘──┘

terraform {
  required_version = ">= 1.6.3"
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "< 2.0.0"
    }
  }
}
