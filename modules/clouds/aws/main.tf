#  ┬ ┬┬─┐┌┐┐┌─┐┌┐┐┬─┐┬─┐
#  │─┤├─  │ ┌─┘│││├─ │┬┘
#  ┘ ┴┴─┘ ┘ └─┘┘└┘┴─┘┘└┘

locals {
  tags = lookup(var.configuration, "tags", {})
}

#  ┬─┐o┬─┐┬─┐┐ ┬┬─┐┬  ┬  
#  ├─ ││┬┘├─ ││││─┤│  │  
#  ┘  ┘┘└┘┴─┘└┴┘┘ ┘┘─┘┘─┘

locals {
  firewall_rules = {
    for name, spec in var.configuration.firewall_rules :
    name => spec if var.create
  }
}

resource "aws_security_group" "this" {
  count  = var.create ? 1 : 0
  name   = var.name
  vpc_id = var.vpc_id

  dynamic "ingress" {
    for_each = {
      for k, v in local.firewall_rules : k => v if v.type == "ingress"
    }
    iterator = rule
    content {
      description      = coalesce(rule.value.description, rule.key)
      from_port        = rule.value.from_port
      to_port          = rule.value.to_port
      protocol         = rule.value.protocol
      cidr_blocks      = rule.value.cidr_blocks_ipv4
      ipv6_cidr_blocks = rule.value.cidr_blocks_ipv6
    }
  }

  dynamic "egress" {
    for_each = {
      for k, v in local.firewall_rules : k => v if v.type == "egress"
    }
    iterator = rule
    content {
      description      = coalesce(rule.value.description, rule.key)
      from_port        = rule.value.from_port
      to_port          = rule.value.to_port
      protocol         = rule.value.protocol
      cidr_blocks      = rule.value.cidr_blocks_ipv4
      ipv6_cidr_blocks = rule.value.cidr_blocks_ipv6
    }
  }

  dynamic "egress" {
    for_each = length([for k, v in local.firewall_rules : k if v.type == "egress"]) == 0 ? toset([1]) : toset([])
    content {
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }
  }

  tags = merge(local.tags, {
    Name = var.name
  })
}

#  ┐─┐┬─┐┬─┐┐ ┬┬─┐┬─┐
#  └─┐├─ │┬┘│┌┘├─ │┬┘
#  ──┘┴─┘┘└┘└┘ ┴─┘┘└┘

locals {
  servers = {
    for server, configuration in var.configuration.servers :
    lookup(configuration, "name", "${var.name}-${server}") => merge(
      {
        location = try(data.aws_subnet.default[0].id, "")
        size     = "t2.micro"
        image    = try(data.aws_ami.ubuntu[0].id, "")
      },
      configuration
    )
    if var.create
  }
}

module "instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "< 6.0.0"

  for_each = local.servers
  name     = each.key
  ami      = each.value.image

  instance_type               = each.value.size
  monitoring                  = false
  vpc_security_group_ids      = try([aws_security_group.this[0].id], [])
  subnet_id                   = each.value.location
  user_data_replace_on_change = true
  user_data                   = each.value.user_data

  create_iam_instance_profile = true
  iam_role_description        = "AmazonSSMManagedInstanceCore"
  iam_role_policies = {
    AmazonSSMManagedInstanceCore = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
  }

  tags = merge(local.tags, {
    Name = each.key
  })
}
