#  ┬─┐┬─┐┌┐┐┬─┐
#  │ ││─┤ │ │─┤
#  ┘─┘┘ ┘ ┘ ┘ ┘

data "aws_ami" "ubuntu" {
  count       = var.create ? 1 : 0
  most_recent = true
  owners      = ["amazon"]
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }
}

data "aws_vpc" "this" {
  count = var.create ? 1 : 0
  state = "available"
  id    = var.vpc_id
}

data "aws_subnets" "all" {
  count = var.create ? 1 : 0
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.this[0].id]
  }
}

data "aws_subnet" "default" {
  count = var.create ? 1 : 0
  id    = data.aws_subnets.all[0].ids[0]
}
