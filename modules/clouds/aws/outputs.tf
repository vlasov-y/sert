#  ┌─┐┬ ┐┌┐┐┬─┐┬ ┐┌┐┐┐─┐
#  │ ││ │ │ │─┘│ │ │ └─┐
#  ┘─┘┘─┘ ┘ ┘  ┘─┘ ┘ ──┘

output "instance" {
  description = "Instances outputs"
  value       = module.instance
}

output "security_group_id" {
  description = "Security group ID if you want to define some rules outside of the module"
  value       = try(aws_security_group.this[0].id, null)
}
