#  ┐ │┬─┐┬─┐┐ ┬
#  ┌┼┘│┬┘│─┤└┌┘
#  ┘ └┘└┘┘ ┘ ┘ 

variable "inbound" {
  type        = string
  description = "Inbound type"
}

variable "inbound_settings" {
  type        = string
  description = "Inbound configuration JSON/YAML encoded"
}

variable "outbound" {
  type        = string
  description = "Outbound type"
}

variable "outbound_settings" {
  type        = string
  description = "Outbound configuration JSON/YAML encoded"
}
