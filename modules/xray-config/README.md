# xray-config

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.6.3 |

## Providers

No providers.

## Modules

No modules.

## Resources

No resources.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_inbound"></a> [inbound](#input\_inbound) | Inbound type | `string` | n/a | yes |
| <a name="input_inbound_settings"></a> [inbound\_settings](#input\_inbound\_settings) | Inbound configuration JSON/YAML encoded | `string` | n/a | yes |
| <a name="input_outbound"></a> [outbound](#input\_outbound) | Outbound type | `string` | n/a | yes |
| <a name="input_outbound_settings"></a> [outbound\_settings](#input\_outbound\_settings) | Outbound configuration JSON/YAML encoded | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_result"></a> [result](#output\_result) | Result config |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
