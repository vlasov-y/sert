#  ┐ │┬─┐┬─┐┐ ┬
#  ┌┼┘│┬┘│─┤└┌┘
#  ┘ └┘└┘┘ ┘ ┘ 

locals {
  inbounds  = jsondecode(templatefile("${path.module}/templates/inbounds/${var.inbound}.json", jsondecode(var.inbound_settings)))
  outbounds = jsondecode(templatefile("${path.module}/templates/outbounds/${var.outbound}.json", jsondecode(var.outbound_settings)))
}

output "result" {
  description = "Result config"
  value = {
    "log" : {
      "loglevel" : "debug"
    },
    "routing" : {
      "rules" : [],
      "domainStrategy" : "AsIs",
      "domainMatcher" : "hybrid"
    },
    "inbounds" : local.inbounds,
    "outbounds" : local.outbounds
  }
}
