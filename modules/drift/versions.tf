#  ┐ ┬┬─┐┬─┐┐─┐o┌─┐┌┐┐┐─┐
#  │┌┘├─ │┬┘└─┐││ ││││└─┐
#  └┘ ┴─┘┘└┘──┘┘┘─┘┘└┘──┘

terraform {
  required_version = ">= 1.6.3"
  required_providers {
    random = {
      source  = "hashicorp/random"
      version = "< 4.0.0"
    }
    time = {
      source  = "hashicorp/time"
      version = "< 1.0.0"
    }
  }
}
