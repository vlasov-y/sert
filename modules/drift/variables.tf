#  ┐ ┬┬─┐┬─┐o┬─┐┬─┐┬  ┬─┐┐─┐
#  │┌┘│─┤│┬┘││─┤│─││  ├─ └─┐
#  └┘ ┘ ┘┘└┘┘┘ ┘┘─┘┘─┘┴─┘──┘

variable "create" {
  type        = bool
  description = "Main switch"
  default     = true
}

variable "minutes" {
  type        = number
  description = "Rotation minutes"
  default     = 0
  validation {
    condition     = var.minutes >= 0
    error_message = "Minutes must be >= 0."
  }
}

variable "hours" {
  type        = number
  description = "Rotation hours"
  default     = 0
  validation {
    condition     = var.hours >= 0
    error_message = "Hours must be >= 0."
  }
}

variable "days" {
  type        = number
  description = "Rotation days"
  default     = 0
  validation {
    condition     = var.days >= 0
    error_message = "Days must be >= 0."
  }
}

variable "drift_percent" {
  type    = number
  default = 0
  validation {
    condition     = var.drift_percent >= 0
    error_message = "Values must be >= 0. If 0 - drift is disabled."
  }
}
