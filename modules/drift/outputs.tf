#  ┌─┐┬ ┐┌┐┐┬─┐┬ ┐┌┐┐┐─┐
#  │ ││ │ │ │─┘│ │ │ └─┐
#  ┘─┘┘─┘ ┘ ┘  ┘─┘ ┘ ──┘

output "rotation_rfc3339" {
  value = local.rotation_enabled ? time_rotating.this[0].rotation_rfc3339 : "never"
}
