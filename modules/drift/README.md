# drift

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.6.3 |
| <a name="requirement_random"></a> [random](#requirement\_random) | < 4.0.0 |
| <a name="requirement_time"></a> [time](#requirement\_time) | < 1.0.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_random"></a> [random](#provider\_random) | < 4.0.0 |
| <a name="provider_time"></a> [time](#provider\_time) | < 1.0.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [random_integer.drift](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/integer) | resource |
| [random_shuffle.direction](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/shuffle) | resource |
| [time_rotating.this](https://registry.terraform.io/providers/hashicorp/time/latest/docs/resources/rotating) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_create"></a> [create](#input\_create) | Main switch | `bool` | `true` | no |
| <a name="input_days"></a> [days](#input\_days) | Rotation days | `number` | `0` | no |
| <a name="input_drift_percent"></a> [drift\_percent](#input\_drift\_percent) | n/a | `number` | `0` | no |
| <a name="input_hours"></a> [hours](#input\_hours) | Rotation hours | `number` | `0` | no |
| <a name="input_minutes"></a> [minutes](#input\_minutes) | Rotation minutes | `number` | `0` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_rotation_rfc3339"></a> [rotation\_rfc3339](#output\_rotation\_rfc3339) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
