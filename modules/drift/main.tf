#  ┬─┐┬─┐o┬─┐┌┐┐
#  │ ││┬┘│├─  │ 
#  ┘─┘┘└┘┘┘   ┘ 

locals {
  total            = var.minutes + var.hours * 60 + var.days * 1440
  rotation_enabled = var.create && local.total > 0
  drift_enabled    = var.create && var.drift_percent > 0
  drift            = try(random_shuffle.direction[0].result == "up" ? random_integer.drift[0].result : random_integer.drift[0].result * -1, 0)
  rotation_time    = ceil(local.total * ((100 + local.drift) / 100))
}

resource "random_shuffle" "direction" {
  count        = local.rotation_enabled && local.drift_enabled ? 1 : 0
  input        = ["up", "down"]
  result_count = 1
}

resource "random_integer" "drift" {
  count = local.rotation_enabled && local.drift_enabled ? 1 : 0
  min   = 1
  max   = var.drift_percent
}

resource "time_rotating" "this" {
  count            = local.rotation_enabled ? 1 : 0
  rotation_minutes = local.rotation_time
  triggers = {
    time = local.rotation_time
  }
}
