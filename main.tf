#  ┌┌┐┬─┐o┌┐┐
#  ││││─┤││││
#  ┘ ┘┘ ┘┘┘└┘

locals {
  name = var.environment

  clouds = {
    hcloud = 1
    aws    = 1
  }
  ssh_port = 54772

  servers = merge([
    for cloud, count in local.clouds :
    {
      for i in range(count) :
      "${cloud}-${i}" => {
        name  = "${cloud}-${i}"
        cloud = cloud
      }
    }
  ]...)

  mesh_names = random_shuffle.mesh.result
  // Compose a mesh chain
  mesh = {
    for i, name in local.mesh_names :
    "${name}" => merge(local.servers[name], {
      is_first = (i == 0)
      index    = i
      is_last  = (i == length(local.mesh_names) - 1)

      prev = i > 0 ? local.servers[local.mesh_names[i - 1]] : {}
      next = i < length(local.mesh_names) - 1 ? local.servers[local.mesh_names[i + 1]] : {}

      xray_uuid = random_uuid.xray[name].result
      server_id = random_string.server_id[name].result
      fqdn      = "${random_string.server_id[name].result}.${local.domain}"
    })

  }

  firewall_rules = [
    {
      type             = "ingress"
      from_port        = local.ssh_port
      to_port          = local.ssh_port
      protocol         = "tcp"
      description      = "Nothing special"
      cidr_blocks_ipv4 = ["0.0.0.0/0"]
      cidr_blocks_ipv6 = ["::/0"]
    },
    {
      type             = "ingress"
      from_port        = 80
      to_port          = 80
      protocol         = "tcp"
      description      = "HTTP"
      cidr_blocks_ipv4 = ["0.0.0.0/0"]
      cidr_blocks_ipv6 = ["::/0"]
    },
    {
      type             = "ingress"
      from_port        = 443
      to_port          = 443
      protocol         = "tcp"
      description      = "HTTPS"
      cidr_blocks_ipv4 = ["0.0.0.0/0"]
      cidr_blocks_ipv6 = ["::/0"]
    }
  ]

  configuration = {
    for cloud, count in local.clouds :
    "${cloud}" => {
      servers = {
        for name, spec in local.servers :
        name => {
          name      = spec.name
          user_data = module.cloudinit[name].result
        }
        if spec.cloud == cloud
      }
      firewall_rules = local.firewall_rules
      tags           = local.tags
    }
  }

  tags = {
    Environment  = var.environment
    Organization = var.organization
  }
}

module "rotation" {
  source        = "./modules/drift"
  for_each      = local.servers
  hours         = 1
  drift_percent = 50
}

module "xray_config" {
  source   = "./modules/xray-config"
  for_each = local.servers
  inbound  = "VLESS-TCP-TLS-WS"
  outbound = local.mesh[each.key].is_last ? "freedom" : "VLESS-TCP-TLS"

  inbound_settings = jsonencode({
    clients = local.mesh[each.key].is_first ? var.xray_vless_clients : [{
      id    = local.mesh[local.mesh[each.key].prev.name].xray_uuid,
      email = "${local.mesh[local.mesh[each.key].prev.name].xray_uuid}@example.com"
    }]
    fallback_site_destination = "127.0.0.1:80"
    websocket_host_header     = local.mesh[each.key].fqdn
    websocket_uri_path        = local.websocket_uri_path
    ca_path                   = "/etc/xray/ca.pem"
    certificate_path          = "/etc/xray/cert.pem"
    key_path                  = "/etc/xray/key.pem"
  })
  outbound_settings = jsonencode({
    address = try(local.mesh[local.mesh[each.key].next.name].fqdn, "")
    port    = 443
    ca_path = "/etc/xray/ca.pem"
    users = [{
      id         = local.mesh[each.key].xray_uuid,
      encryption = "none",
      level      = 0,
    }]
  })
}

#  ┌─┐┬  ┌─┐┬ ┐┬─┐o┌┐┐o┌┐┐
#  │  │  │ ││ ││ ││││││ │ 
#  └─┘┘─┘┘─┘┘─┘┘─┘┘┘└┘┘ ┘ 

module "cloudinit" {
  source          = "./modules/cloudinit"
  for_each        = local.servers
  username        = "bzm"
  password        = "$1$cStafYdT$aofydZSFQuSOBNV.U5Y9B."
  ssh_port        = local.ssh_port
  ssh_public_keys = ["ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPLJ0f3vAAQ9hpaqqo3ozOlLhhaKCoqwr6QGSzCUvdcz y.vlasov"]

  runcmd = [<<-EOF
    echo 'Next rotation at ${module.rotation[each.key].rotation_rfc3339}'
    cd /opt
    docker compose up -d
  EOF
  ]
  files = {
    "/opt/docker-compose.yml" = <<-EOF
      services:
        xray:
          image: teddysun/xray:1.8.6
          restart: always
          network_mode: host
          volumes:
          - /etc/xray:/etc/xray:ro
          - /etc/ssl/certs:/etc/ssl/certs:ro
          logging:
            driver: json-file
            options:
              max-size: "100m"
              max-file: "1"
        nginx:
          image: nginxinc/nginx-unprivileged:1.25.3-alpine3.18
          restart: always
          ports:
          - 80:8080
          logging:
            driver: json-file
            options:
              max-size: "100m"
              max-file: "1"
    EOF
    "/etc/xray/key.pem" = (
      local.mesh[each.key].is_first ?
      # First server must use a valid TLS...
      acme_certificate.entrypoint.private_key_pem :
      # ...while others - self-signed
      tls_private_key.chain.private_key_pem
    )
    "/etc/xray/cert.pem" = (
      local.mesh[each.key].is_first ?
      # First server must use a valid TLS...
      format("%s%s",
        acme_certificate.entrypoint.certificate_pem,
        acme_certificate.entrypoint.issuer_pem
      ) :
      # ...while others - self-signed
      format("%s%s",
        tls_locally_signed_cert.chain.cert_pem,
        tls_self_signed_cert.ca.cert_pem
      )
    )
    # CA to verify selfsigned inbound and server certificate in outboud connections
    "/etc/xray/ca.pem"      = tls_self_signed_cert.ca.cert_pem
    "/etc/xray/config.json" = jsonencode(module.xray_config[each.key].result)
  }
}

#  ┐─┐┬─┐┬─┐┐ ┬┬─┐┬─┐┐─┐
#  └─┐├─ │┬┘│┌┘├─ │┬┘└─┐
#  ──┘┴─┘┘└┘└┘ ┴─┘┘└┘──┘

module "hcloud" {
  source = "./modules/clouds/hcloud"
  create = lookup(local.clouds, "hcloud", 0) > 0

  name          = local.name
  configuration = local.configuration.hcloud
}

data "aws_vpc" "default" {
  state   = "available"
  default = true
}

module "aws" {
  source = "./modules/clouds/aws"
  create = lookup(local.clouds, "aws", 0) > 0

  vpc_id        = data.aws_vpc.default.id
  name          = local.name
  configuration = local.configuration.aws
}

#  ┬─┐┌┐┐┐─┐
#  │ ││││└─┐
#  ┘─┘┘└┘──┘

locals {
  domain = "vlasov.pro"
}

data "cloudflare_zone" "this" {
  name = local.domain
}

resource "cloudflare_record" "hcloud" {
  for_each = local.configuration.hcloud.servers
  zone_id  = data.cloudflare_zone.this.id
  name     = "${random_string.server_id[each.key].result}.${local.domain}"
  type     = "A"
  value    = module.hcloud.server[each.key].ipv4_address
  ttl      = 60
  proxied  = false
}

resource "cloudflare_record" "aws" {
  for_each = local.configuration.aws.servers
  zone_id  = data.cloudflare_zone.this.id
  name     = "${random_string.server_id[each.key].result}.${local.domain}"
  type     = "A"
  value    = module.aws.instance[each.key].public_ip
  ttl      = 60
  proxied  = false
}

#  ┌┐┐┬  ┐─┐
#   │ │  └─┐
#   ┘ ┘─┘──┘

resource "tls_private_key" "ca" {
  algorithm = "ED25519"
}

resource "tls_self_signed_cert" "ca" {
  private_key_pem = tls_private_key.ca.private_key_pem
  subject {
    common_name = "*.${local.domain}"
  }
  is_ca_certificate     = true
  dns_names             = ["*.${local.domain}"]
  validity_period_hours = 8760
  early_renewal_hours   = 720
  allowed_uses = [
    "cert_signing",
    "digital_signature",
    "key_encipherment"
  ]
}

resource "tls_private_key" "chain" {
  algorithm = "ED25519"
}

resource "tls_cert_request" "chain" {
  private_key_pem = tls_private_key.chain.private_key_pem
  subject {
    common_name = "*.${local.domain}"
  }
  dns_names = ["*.${local.domain}"]
}

resource "tls_locally_signed_cert" "chain" {
  cert_request_pem      = tls_cert_request.chain.cert_request_pem
  ca_private_key_pem    = tls_private_key.ca.private_key_pem
  ca_cert_pem           = tls_self_signed_cert.ca.cert_pem
  validity_period_hours = 8760
  allowed_uses = [
    "key_encipherment",
    "digital_signature",
    "server_auth",
    "client_auth"
  ]
}

#  ┬─┐┌─┐┌┌┐┬─┐
#  │─┤│  │││├─ 
#  ┘ ┘└─┘┘ ┘┴─┘

resource "tls_private_key" "acme_account" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "acme_registration" "this" {
  account_key_pem = tls_private_key.acme_account.private_key_pem
  email_address   = "admin@${local.domain}"
}

resource "acme_certificate" "entrypoint" {
  account_key_pem           = acme_registration.this.account_key_pem
  common_name               = local.mesh[local.mesh_names[0]].fqdn
  subject_alternative_names = [local.mesh[local.mesh_names[0]].fqdn]
  key_type                  = "P384"
  min_days_remaining        = 5
  dns_challenge {
    provider = "cloudflare"
    config = {
      CF_DNS_API_TOKEN = var.cloudflare_token
    }
  }
}
