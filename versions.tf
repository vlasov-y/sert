#  ┐ ┬┬─┐┬─┐┐─┐o┌─┐┌┐┐┐─┐
#  │┌┘├─ │┬┘└─┐││ ││││└─┐
#  └┘ ┴─┘┘└┘──┘┘┘─┘┘└┘──┘

terraform {
  required_version = ">= 1.6.3"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "< 6.0.0"
    }
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "< 2.0.0"
    }
    local = {
      source  = "hashicorp/local"
      version = "< 3.0.0"
    }
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "< 5.0.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "< 4.0.0"
    }
    acme = {
      source  = "vancluever/acme"
      version = "< 3.0.0"
    }
    tls = {
      source  = "hashicorp/tls"
      version = "< 5.0.0"
    }
  }
}

#  ┬─┐┬─┐┌─┐┐ ┬o┬─┐┬─┐┬─┐┐─┐
#  │─┘│┬┘│ ││┌┘││ │├─ │┬┘└─┐
#  ┘  ┘└┘┘─┘└┘ ┘┘─┘┴─┘┘└┘──┘

provider "hcloud" {
  token = var.hcloud_token
}

provider "aws" {
  region = "eu-central-1"
  default_tags {
    tags = local.tags
  }
}

provider "cloudflare" {
  api_token = var.cloudflare_token
}

provider "acme" {
  # server_url = "https://acme-staging-v02.api.letsencrypt.org/directory"
  server_url = "https://acme-v02.api.letsencrypt.org/directory"
}
