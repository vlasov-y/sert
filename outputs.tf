#  ┌─┐┬ ┐┌┐┐┬─┐┬ ┐┌┐┐┐─┐
#  │ ││ │ │ │─┘│ │ │ └─┐
#  ┘─┘┘─┘ ┘ ┘  ┘─┘ ┘ ──┘

output "domains" {
  description = "Domains created for servers"
  value = merge(
    { for key, value in cloudflare_record.hcloud : key => value.name },
    { for key, value in cloudflare_record.aws : key => value.name }
  )
}

output "mesh" {
  description = "Mesh chain"
  value       = local.mesh_names
}

output "xray_config" {
  sensitive   = true
  description = "XRay configuration for all nodes in the chain"
  value = {
    for key, value in module.xray_config :
    key => value.result
  }
}

output "xray_config_json" {
  sensitive   = true
  description = "Like xray_config but in JSON"
  value = jsonencode({
    for key, value in module.xray_config :
    key => value.result
  })
}

output "websocket_uri_path" {
  description = "Websocket URI path to use"
  value       = local.websocket_uri_path
}

output "entrypoint" {
  description = "Chain entrypoint domain name"
  value       = local.mesh[local.mesh_names[0]].fqdn
}

output "ca_cert_pem" {
  description = "CA we used for issuing server chain certificates"
  value       = tls_self_signed_cert.ca.cert_pem
}
